/*******************************************************************************
 * 																	LISTESCHAINEES.C
 *
 * Contient l'implementation des fonctions du fichier listesChainees.h
 * qui permettent de manipuler la liste de messages
 *
 ******************************************************************************/

#include "listesChainees.h"

/*******************************************************************************
 * Cree une cellule contenant le message et les dates en argument
 *
 * @param message:		Message affecte a la cellule cree
 * @param dateDebut:	Date "d'envoi" du message
 * @param dateFin:		Date "d'expiration" du message
 * @return 						Adresse de la cellule cree
 ******************************************************************************/
cellule_t * creerCellule(char message[100],int dateDebut, int dateFin){

	cellule_t	*	nouveau	= (cellule_t *) malloc (sizeof(cellule_t));

	if(nouveau == NULL)
	{
		fprintf(stderr, "creerCellule(): Echec allocation\n");
	}
	else
	{
		nouveau->dateDebut	= dateDebut;
		nouveau->dateFin		= dateFin;
		strcpy(nouveau->message,message);

		nouveau->suivant		= NULL;
	}

	return nouveau;
}

/*******************************************************************************
 * Recherche la derniere cellule dont la date de debut est inferieure a la date
 * passee en parametre
 *
 * @param pliste:	Adresse du pointeur de tete de la liste a parcourir
 * @param date:		Date a rechercher
 * @return 				Adresse de l'attribut 'suivant' de la cellule retenue
 ******************************************************************************/
cellule_t		**	rechercherDateDebutPrecedente(cellule_t ** pliste, int date)
{
	cellule_t	**	prec	= pliste;	/* On debut sur le premier element de la liste */

	/* Parcours tant que la liste n'est pas terminee et que la date n'est pas trouvee */
	while( *prec != NULL	&&	(*prec)->dateDebut < date)
	{
		prec	=	&((*prec)->suivant);	/* Passage a l'element suivant */
	}

	return prec;
}

/*******************************************************************************
 * Insert la cellule passee en parametre dans la liste en repectant un ordre
 * croissant sur les attributs 'dateDebut' des cellules grace a la position
 * donnee par la fonction de recherche de precedent
 *
 * @param pliste:		Adresse du pointeur de tete de la liste a parcourir
 * @param cellule:	Cellule a inserer
 ******************************************************************************/
void insererCellule(cellule_t ** pliste, cellule_t * cellule)
{
		cellule_t ** prec		= rechercherDateDebutPrecedente(pliste, cellule->dateDebut);
		cellule->suivant		= *prec;
		*prec								= cellule;
}

/*******************************************************************************
 * Supprime et libere la cellule pointee par celle passee en parametre
 *
 * @param prec:	Adresse de l'attribut 'suivant' de la cellule precedent celle a
 *							supprimer
 ******************************************************************************/
void supprimerCellule(cellule_t ** prec)
{
	cellule_t	*	tmp	= *prec;
	*prec						= (*prec)->suivant;
	free(tmp);
}

/*******************************************************************************
 * Affiche les trois attributs de la cellule en argument dans le 'flux' specifie
 *
 * @param flux:			Flux cible (un fichier ou stdout) pour l'ecriture des infos
 * @param cellule:	Cellule a afficher
 ******************************************************************************/
void ecrireCellule(FILE * flux, cellule_t * cellule)
{
	/* affiche l'adresse de la cellule puis l'adresse de son suivant */
	/* printf("%p -> %p ", (void *) cellule, (void *) cellule->suivant); */	/* DEBUG */

	fprintf(flux, "%d ", cellule->dateDebut);
	fprintf(flux, "%d ", cellule->dateFin);
	fprintf(flux, "%s\n", cellule->message);
}

/*******************************************************************************
 * Utilise 'ecrireCellule()' pour afficher chacune des cellules de la liste
 * en verifiant au prealable le cas de la liste vide
 *
 * @param flux:		Flux cible (un fichier ou stdout) pour l'ecriture des infos
 * @param liste:	Liste a afficher
 ******************************************************************************/
void ecrireListe (FILE * flux, cellule_t * liste)
{
	cellule_t	* parcourt	= liste;

	if (parcourt == NULL)
	{
		printf("Liste vide\n");
	}
	else
	{
		while(parcourt)
		{
			ecrireCellule(flux, parcourt);
			parcourt = parcourt->suivant;
		}
	}
}

/*******************************************************************************
 * Fonction specifique destinee a afficher simplement une liste sur STDOUT
 *
 * @param liste:	Liste a afficher
 ******************************************************************************/
void afficherListe (cellule_t * liste)
{
	ecrireListe(stdout, liste);
}

/*******************************************************************************
 * Importe une liste depuis le fichier de nom 'nomFichier', genere une liste
 * triee sur la date de debut et retourne l'adresse de la premiere cellule
 *
 * @param nomFichier:	Nom du fichier a importer
 * @return						Adresse de la premiere cellule de la list importee
 ******************************************************************************/
cellule_t	* importerListe(char * nomFichier)
{
	cellule_t	* liste					= NULL;
	cellule_t	* temp					= NULL;	/* Cellule temporaire */
	int					dateDebut			= 0;
	int					dateFin				= 0;
	char				message[100]	= {0};
	FILE 			* f							= fopen(nomFichier, "r");
	int					n							= 0;		/* Recupere le nombre de caracteres lus */

	if(f == NULL)
	{
		printf("Fichier inconnu\n");
	}
	else
	{
		printf(" \"%s\" ouvert\n", nomFichier);
		while( ! feof(f) ) 													/* Lecture des lignes jusqu'a fin de fichier */
		{
			fscanf(f, "%d %d", &dateDebut, &dateFin);

			n = fscanf(f, " %100[^\n]", message);			/* lecture jusqu'a '\n' */

			if(n != -1) 															/* On verifie qu'on a lu un message */
			{
				temp	= creerCellule(message, dateDebut, dateFin);
				if(temp)	insererCellule(&liste, temp);	/* Insertion si creation reussie */
			}
		}
		fclose(f);
	}
	return liste;
}

/*******************************************************************************
 * Sauvegarde la liste passee en argument dans le fichier specifie qui sera cree
 * s'il n'existe pas encore
 *
 * @param nomFichier:	Nom du fichier dans lequel sauvegarder le contenu de liste
 * @param liste:			Adresse de la liste a sauvegarder
 ******************************************************************************/
void sauvegardeFichier (char * nomFichier, cellule_t * liste)
{
	FILE * f		=	fopen(nomFichier, "w");

	if(f == NULL)
	{
		fprintf(stderr,"sauvegardeFichier(): Echec de l'ouverture du fichier");
	}
	else
	{
		ecrireListe(f, liste);		/* Ecrit la liste dans le flux 'f' */
		fclose(f);
	}
}

/*******************************************************************************
 * Execute et retourne le resultat de la commande systeme  'date +%Y%m%d'
 *
 * @return	Date du jour au format aaaammjj
 ******************************************************************************/
int dateDuJour()
{
	int 		dateDuJour	= 0;
	FILE *	fdate				= popen("date +%Y%m%d", "r");

	if(fdate)
	{
		fscanf(fdate, "%d", &dateDuJour);
		pclose(fdate);
	}
	else
	{
		puts("Echec de lecture de la date");
	}

	return dateDuJour;
}

/*******************************************************************************
 * Supprime les cellules dont la date de fin a depasse la date du jour
 *
 * @param pliste:	Adresse du pointeur de tete de liste
 ******************************************************************************/
void expirationMessages(cellule_t ** pliste)
{
	cellule_t	**	prec					= pliste;
	int						dateActuelle	= dateDuJour();	/* Recuperation de la date du jour */

	if(dateActuelle)	/* Execution uniquement si la dateDuJour est non nulle */
	{
		while(* prec)
		{
			if ((*prec)->dateFin < dateActuelle)	supprimerCellule(prec);
			else 																	prec = &((*prec)->suivant);
		}
	}
}

/*******************************************************************************
 * Remplace la date des cellules dont dateDebut correspond a 'dateAModifier' par
 * la valeur de 'nouvelleDate' et replace l'ensemble des cellules modifiees en
 * respectant la contrainte de tri sur l'attribut dateDebut
 *
 * @param pliste:					Adresse du pointeur de tete de liste
 * @param dateAModifier:	Date a rechercher dans les cellules
 * @param nouvelleDate:		Date a utiliser pour modifier les cellules
 ******************************************************************************/
void modificationDateDebut(cellule_t ** pliste, int dateAModifier, int nouvelleDate)
{
	cellule_t	*		parcourt			= NULL;
	cellule_t	*		dernierModif	= NULL;	/* Adresse de la derniere cellule modifiee */
	cellule_t	*		temp					= NULL;
	cellule_t	**	precA					= NULL;	/* Adresse de la premiere cellule avec une date a modifier */
	cellule_t	**	precB					= NULL;	/* Adresse ou replacer les cellues modifiees ensuite */

	/* Trouver precA de la premiere cellule avec une date correspondante */
	precA	= rechercherDateDebutPrecedente(pliste, dateAModifier);

	/* Si la recherche a trouve un element avec une date a modifier alors */
	if ( (*precA) != NULL && ((*precA)->dateDebut == dateAModifier) )
	{
		/* Trouver le precB de la premiere cellule a la nouvelle date */
		precB			= rechercherDateDebutPrecedente(pliste, nouvelleDate);

		/* Parcourir et modifier toutes les cellules avec une date valide */
		parcourt	= (*precA);
		while(parcourt && parcourt->dateDebut == dateAModifier)
		{
			parcourt->dateDebut	= nouvelleDate;
			dernierModif				= parcourt;
			parcourt 						= parcourt->suivant;
		}

		if(*precB != parcourt) 	/* Sinon: Cas particulier ou l'ordre est similaire -> On ne fait rien */
		{
			/* On conserve l'adresse de debut de la liste modifiee */
			temp									= (*precA);
			/* Branche precA sur le suiv de la derniere cellule modifiee */
			(*precA)							= parcourt;
			/* Placer le suivant de precB dans le suivant de la derniere cellule modifiee */
			dernierModif->suivant	= (*precB);
			/* Faire pointer precB sur temp */
			(*precB)							= temp;
		}
	}
}

/*******************************************************************************
 * Affiche les messages dont la date d'expiration 'dateFin' est inferieure a la
 * date du jour
 *
 * @param liste:	Pointeur de tete de liste
 ******************************************************************************/
void afficherMessagesValides(cellule_t * liste)
{
	cellule_t	*	cour					=	liste;
	int					dateActuelle	=	dateDuJour();

	/* On parcourt l'ensemble des messages dont la date de fin est valide */
	while(cour)
	{
		if(cour->dateFin >= dateActuelle)	ecrireCellule(stdout, cour);
		cour	=	cour->suivant;
	}
}

/*******************************************************************************
 * Libere successivement la memoire de chacune des cellules de la liste
 *
 * @param pliste:	Adresse du pointeur de tete de liste
 ******************************************************************************/
void libererListe(cellule_t ** pliste)
{
	cellule_t	* parcourt	=	*	pliste;
	cellule_t	* tmp				=		parcourt;

	while (parcourt)
	{
		parcourt	= parcourt->suivant;
		free(tmp);
		tmp				= parcourt;
	}

	* pliste = NULL;
}

/*----------------------------------------------------------------------------*/
/*------------------------------- BONUS --------------------------------------*/
/*----------------------------------------------------------------------------*/

/*******************************************************************************
 * Fonction booleenne equivalente a la fonction systeme 'strstr' qui teste
 * dans la chaine 'message' si le 'motif' en argument est present ou pas
 *
 * @param message:	Chaine a parcourir
 * @param motif:		Sous-chaine a retrouver dans la chaine 'message'
 * @return					1 si motif present, 0 sinon
 ******************************************************************************/
int avoirMotif(char * message, char * motif)
{
	int	motifPresent		= 0;																						/* Booleen qui passe a 1 si le motif est present */
	int tailleMotif			= strlen(motif);
	int tailleRestante	= strlen(message);															/* Taille de la partie restante a verifier */
	int	debut						= 0;																						/* Indice de debut de la sous-chaine testee */
	int	i								= 0;																						/* Indice de parcours de la sous-chaine testee */

	while(!motifPresent && tailleMotif <= tailleRestante)
	{
		while(i < tailleMotif -1 && message[debut + i] == motif[i])	i++;	/* Parcours de la sous-chaine */

		if(message[debut + i] == motif[i])	motifPresent	=	1;						/* Test de la valeur de sortie */

		debut						++;
		i								=	0;
		tailleRestante	= (int)strlen( &(message[debut]) );								/* Mise a jour de la taille de la partie restante */
	}

	return motifPresent;
}

/*******************************************************************************
 * Pour l'ensemble des cellules de la liste, teste si leur message contient le
 * 'motif' specifie en argument. Affiche les messages validant le test
 *
 * @param liste:	Pointeur de tete de liste
 * @param motif:	Sous-chaine a retrouver dans les cellules de la liste
 ******************************************************************************/
void afficherMessagesMotif(cellule_t * liste, char * motif)
{
	cellule_t	*	cour	= liste;

	while(cour)
	{
		if(avoirMotif(cour->message, motif))	printf("%s\n", cour->message);
		cour	=	cour->suivant;
	}
}
