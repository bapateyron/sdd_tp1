/*******************************************************************************
 * 																	MAIN.C
 *
 * Fichier main.c qui contient la fonction main qui execute les fonctions test
 *
 ******************************************************************************/

 #include "tests.h"

/*******************************************************************************
 * Fonction principale qui lit le fichier (obligatoire) passe en argument
 * et qui execute l'ensemble des fonctions de tests du fichier tests.c
 *
 ******************************************************************************/
int main(int argc, char * argv[])
{
		char				nomFichier[100]	=	{0};		/* Nom du fichier en argument a ouvrir */
		int					codeRetour			= 0;			/* Valeur pour le return */

		if(argc < 2)
		{
			printf("Erreur: Nombre d'arguments = %d\nExemple: \"prog.exe fichiers/messages_1.txt\"\n", argc);
			codeRetour	=	-1;
		}
		else
		{
			strcpy(nomFichier, argv[1]);				/* Recuperation du nom du fichier */

			/* Appel des fonctions de test du fichier tests.c */
			test_recherchePrecedent(nomFichier);
			test_insertionCellules();
			test_lectureFichier(nomFichier);
			test_sauvegardeFichier(nomFichier);
			test_supprimerCellule(nomFichier);
			test_expirationMessages(nomFichier);
			test_modificationDateDebut(nomFichier);
			test_affichageMessagesValides(nomFichier);
			test_rechercheMotifs(nomFichier);
		}

		return codeRetour;
}
