FLAGS := -Wall -Wextra -ansi -pedantic -g

all: prog.bin

main.o: main.c
	gcc $(FLAGS) -c main.c

tests.o: tests.c
	gcc $(FLAGS) -c tests.c

listesChainees.o: listesChainees.c
		gcc $(FLAGS) -c listesChainees.c

prog.bin: listesChainees.o tests.o main.o
	gcc $(FLAGS) listesChainees.o tests.o main.o -o prog.exe

clean:
	rm *.o *~ vgcore* *.exe fichiers/fichierVide.txt fichiers/sauvegarde.txt
