/*******************************************************************************
 * 																	TESTS.C
 *
 * Fichier contenant l'implementation de toutes les fonctions de test du projet
 * Chaque fonction teste une fonctionnalite specifique du projet.
 * Pour chaque fonction de test, on affiche l'etat de depart (le plus souvent)
 * la liste avant l'appel de la fonction a tester. Puis apres appel de la
 * fonction, on affiche les modifications, le plus souvent en affichant la
 * liste ou le fichier modifie
 *
 ******************************************************************************/

#include "tests.h"

void test_recherchePrecedent(char * nomFichier) /* Activer l'affichage de 'DEBUG' dans ecrireCellule() */
{
	cellule_t	*	liste 			= NULL;

	puts("\n\n\n---- Tests de recherche du precedent (sur dateDebut) ----");
	puts("Liste de depart -----------------------------------------------------");
	puts("\n-- test_recherchePrecedent(): liste vide --");

	printf("%p\n---\n\n", (void *) *rechercherDateDebutPrecedente(&liste, 20190104));

	/* On valorise la liste pour les cas suivants: */
	liste	= importerListe(nomFichier);
	afficherListe(liste);

	puts("\n-- test_recherchePrecedent(): premier element --");
	printf("%p\n", (void *) *rechercherDateDebutPrecedente(&liste, 20180101));

	puts("\n-- test_recherchePrecedent(): dernier element --");
	printf("%p\n", (void *) *rechercherDateDebutPrecedente(&liste, 20180104));

	puts("\n-- test_recherchePrecedent(): absent de la liste --");
	printf("%p\n", (void *) *rechercherDateDebutPrecedente(&liste, 30000000));

	libererListe(&liste);
}

void test_insertionCellules()
{
	cellule_t * liste = NULL;
	puts("\n\n\n---- Tests d'insertion de cellule ----");
	afficherListe(liste);

	/* Insertion sur liste vide */
	puts("\n-- test_insertionCellules(): liste vide --");
	insererCellule(&liste, creerCellule("tu vas bien ?",20190102,20190601));	/* Message n°3 */
	afficherListe(liste);

	/* Insertion sur le dernier element */
	puts("\n-- test_insertionCellules(): insertion sur dernier element --");
	insererCellule(&liste, creerCellule("bien ",20190103,20190601));					/* Message n°4 */
	afficherListe(liste);

	/* Insertion sur premier element */
	puts("\n-- test_insertionCellules(): Insertion sur premier element --");
	insererCellule(&liste, creerCellule("coucou",20190101,20190601));					/* Message n°1 */
	afficherListe(liste);

	puts("\n-- test_insertionCellules(): Insertions en pleine liste --");
	insererCellule(&liste, creerCellule("ok ",20190104,20190601));						/* Message n°5 */
	insererCellule(&liste, creerCellule("salut",20190102,20190601));					/* Message n°2 */
	afficherListe(liste);

	libererListe(&liste);
}

void test_lectureFichier(char * nomFichier)
{
	cellule_t	* liste				= NULL;
	FILE			*	fichierVide	=	NULL;

	puts("\n\n\n---- Tests de lecture de fichier ----");

	/* Cas fichier inexistant */
	puts("\n-- test_lectureFichier(): fichier inexistant --");
	liste				= importerListe("fichierInexistant.txt");

	/* Cas fichier vide */
	puts("\n-- test_lectureFichier(): fichier vide --");
		/* Creation d'un fichier vide */
	fichierVide	=	fopen("fichiers/fichierVide.txt", "w");
	fwrite("", 0, 0, fichierVide);
	liste				= importerListe("fichiers/fichierVide.txt");
	afficherListe(liste);
	fclose(fichierVide);

	/* Cas fichier rempli avec format suppose correct par hypothese */
	puts("\n-- test_lectureFichier(): fichier rempli --");
	liste				= importerListe(nomFichier);
	afficherListe(liste);

	libererListe(&liste);
}

void test_sauvegardeFichier(char * nomFichier)
{
	char			* nomDestination			= "fichiers/sauvegarde.txt";		/* Nom du fichier dans lequel on ecrit */
	char			*	fichierVide					=	"fichiers/fichierVide.txt";		/* Nom du fichier vide */
	cellule_t * liste								= NULL;													/* Liste de la conversation initiale */
	cellule_t * sauvegarde					= NULL;													/* Liste qui recevra le contenu du fichier de sauvegarde */

	puts("\n\n\n---- Tests de sauvegarde de fichier ----");

	printf("\n-- test_sauvegardeFichier(): Sauvegarde de \"%s\" dans \"%s\" --\n", fichierVide, nomDestination);
	liste				= importerListe(fichierVide);
	printf(" /* Ecriture dans \"%s\" */\n", nomDestination);
	sauvegardeFichier(nomDestination, liste);												/* Affiche "Liste vide" */
	sauvegarde	= importerListe(nomDestination);
	afficherListe(sauvegarde);																			/* Affiche "Liste vide" car le fichier est lui aussi normalement vide */
	libererListe(&sauvegarde);
	libererListe(&liste);

	printf("\n-- test_sauvegardeFichier(): Sauvegarde de \"%s\" dans \"%s\" --\n", nomFichier, nomDestination);
	liste				= importerListe(nomFichier);
	afficherListe(liste);
	printf("\n /* Ecriture dans \"%s\" */\n", nomDestination);
	sauvegardeFichier(nomDestination, liste);
	sauvegarde	= importerListe(nomDestination);										/* On recupere le fichier ecrit pour verifier son contenu */
	puts("Liste importee -----------------------------------------------------");
	afficherListe(sauvegarde);																			/* Affichage du contenu du fichier de sauvegarde */

	libererListe(&sauvegarde);
	libererListe(&liste);
}

void test_supprimerCellule(char * nomFichier)
{
	cellule_t	* 	liste							= importerListe(nomFichier);
	cellule_t	*		elementDeFin			= NULL;
	cellule_t	**	adresseElementFin	= NULL;

	puts("\n\n\n---- Tests de suppression de cellule ----");
	puts("\n-- test_supprimerCellule(): suppression du premier element");
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	/* Cas liste vide non traite car il est verifie par la fonction appelante */

	/* Cas suppression 1er element */
	printf("\n /* Suppression de la cellule de debut */\n");
	supprimerCellule(&liste);
	puts("\nResultat ------------------------------------------------------------");
	afficherListe(liste);

	/* Cas suppression element de fin */
	/* (on va ajouter un element a la fin dont on connaitra l'adresse) */
	/* (puis on supprimera ce meme element) */
	puts("\n\n-- test_supprimerCellule(): suppression de l'element de fin --");
	elementDeFin			= creerCellule("Je viens du futur !", 99999999, 99999999);	/* Plus grande date possible */
	insererCellule(&liste, elementDeFin);
	adresseElementFin	= rechercherDateDebutPrecedente(&liste, 99999999);					/* On cible le dernier element */
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* Suppression de la cellule de fin */\n");
	supprimerCellule(adresseElementFin);
	puts("Resultat ------------------------------------------------------------");
	afficherListe(liste);
	libererListe(&liste);

	/* Cas suppression dernier element restant */
	/* (on va creer une liste d'un seul element puis on le supprimera) */
	puts("\n\n-- test_supprimerCellule(): suppression du dernier element --");
	elementDeFin			= creerCellule("Je suis tout seul", 20180101, 20190101);
	insererCellule(&liste, elementDeFin);
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* Suppression de la derniere cellule*/\n");
	supprimerCellule(&liste);
	puts("Resultat ------------------------------------------------------------");
	afficherListe(liste);

	libererListe(&liste);
}

void test_expirationMessages(char * nomFichier)
{
	cellule_t * liste	= NULL;

	puts("\n\n\n---- Tests de la fonction d'expiration des messages ----");

	/* On utilisera la date du jour: */
	printf("\n\t-- Date du jour utilisee: [%d] --\n\n", dateDuJour());

	/* Cas aucune date n'expire */
	puts("\n-- test_expirationMessages(): aucun message ne doit expirer avant 2022 --");
	liste	= importerListe("fichiers/aucuneExpiration.txt");
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle expirationMessages() */\n");
	puts("Resultat ------------------------------------------------------------");
	expirationMessages(&liste);
	afficherListe(liste);
	libererListe(&liste);

	/* Cas toutes les dates expirent */
	/* (couvre egalement les cas de suppression du premier et du dernier element) */
	puts("\n\n-- test_expirationMessages(): tous les messages doivent expirer en 2018 --");
	liste	=	importerListe("fichiers/queDesExpirations.txt");
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle expirationMessages() */\n");
	puts("Resultat ------------------------------------------------------------");
	expirationMessages(&liste);
	afficherListe(liste);
	libererListe(&liste);

	/* Cas mixte */
	puts("\n\n-- test_expirationMessages(): cas mixte, quelques messages expirent --");
	liste	=	importerListe(nomFichier);
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle expirationMessages() */\n");
	puts("Resultat ------------------------------------------------------------");
	expirationMessages(&liste);
	afficherListe(liste);

	libererListe(&liste);
}

void test_affichageMessagesValides(char * nomFichier)
{
	cellule_t * liste	= NULL;
	puts("\n\n\n---- Tests d'affichage des messages valides ----");

	printf("\n\t-- Date du jour utilisee: [%d] --\n\n", dateDuJour());
	puts("\n-- test_afficherMessagesValides(): cas d'utilisation general, un seul message encore valide --");
	liste		= importerListe(nomFichier);
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle afficherMessagesValides() */\n");
	puts("Resultat ------------------------------------------------------------");
	afficherMessagesValides(liste);
	libererListe(&liste);
}

void test_modificationDateDebut(char * nomFichier)
{
	cellule_t * liste		= NULL;

	puts("\n\n\n---- Tests sur la modification de date de debut ----");

	/* Cas ou la date n'est pas presente dans la liste */
	puts("\n\n-- test_modificationDateDebut(): date non presente dans la liste --");
	liste	= importerListe(nomFichier);
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle modificationDateDebut() -- aucun changement */\n");
	modificationDateDebut(&liste, 99999999, 11111111);	/* 99999999 n'est pas presente */
	puts("Resultat ------------------------------------------------------------");
	afficherListe(liste);
	libererListe(&liste);

	/* Cas ou toute la liste est a la meme date et qu'elle doit etre modifiee */
	puts("\n\n-- test_modificationDateDebut(): toutes les dates de debut sont a modifier --");
	liste	= importerListe("fichiers/datesDebutEgales.txt");
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle modificationDateDebut() */\n");
	modificationDateDebut(&liste, 20180105, 44444444);
	puts("Resultat ------------------------------------------------------------");
	afficherListe(liste);
	libererListe(&liste);

	/* Cas ou le premier element de la liste est a modifier */
	puts("\n\n-- test_modificationDateDebut(): le premier element est a modifier");
	liste	= importerListe(nomFichier);
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle modificationDateDebut() */\n");
	modificationDateDebut(&liste, 20180101, 12345678);
	puts("Resultat ------------------------------------------------------------");
	afficherListe(liste);
	libererListe(&liste);

	/* Cas ou le dernier element est a modifier */
	puts("\n\n-- test_modificationDateDebut(): le dernier element est a modifier --");
	liste	= importerListe(nomFichier);
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle modificationDateDebut() */\n");
	modificationDateDebut(&liste, 20180105, 44444444);
	puts("Resultat ------------------------------------------------------------");
	afficherListe(liste);
	libererListe(&liste);

	/* Cas ou les elements modifies passent au tout debut de la liste */
	puts("\n\n-- test_modificationDateDebut(): les elements modifies passent au debut de la liste --");
	liste	= importerListe(nomFichier);
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle modificationDateDebut() */\n");
	modificationDateDebut(&liste, 20180103, 10000000);
	puts("Resultat ------------------------------------------------------------");
	afficherListe(liste);
	libererListe(&liste);

	/* Cas ou les elements modifies passent a la toute fin de la liste */
	puts("\n\n-- test_modificationDateDebut(): les elements modifies passent a la toute fin de la liste --");
	liste	= importerListe(nomFichier);
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle modificationDateDebut() */\n");
	modificationDateDebut(&liste, 20180103, 50000000);
	puts("Resultat ------------------------------------------------------------");
	afficherListe(liste);
	libererListe(&liste);

	/* Cas ou la nouvelle date reste a la meme position dans la liste */
	puts("\n\n-- test_modificationDateDebut(): la nouvelle date est a la meme position --");
	liste	= importerListe(nomFichier);
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle modificationDateDebut() */\n");
	modificationDateDebut(&liste, 20180103, 20180104);
	puts("Resultat ------------------------------------------------------------");
	afficherListe(liste);
	libererListe(&liste);

	/* Cas mixte */
	puts("\n\n-- test_modificationDateDebut(): cas mixte --");
	liste	= importerListe(nomFichier);
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);
	printf("\n /* On appelle modificationDateDebut() */\n");
	modificationDateDebut(&liste, 20180103, 20180102);
	puts("Resultat ------------------------------------------------------------");
	afficherListe(liste);
	libererListe(&liste);

}

void test_rechercheMotifs(char * nomFichier)
{
	cellule_t * liste	= importerListe(nomFichier);

	puts("\n\n\n---- Tests sur la recherche de motifs ----");
	puts("Liste de depart -----------------------------------------------------");
	afficherListe(liste);

	puts("\n-- test_rechercheMotifs(): motif absent --");
	printf("\n /* On appelle rechercheMotifs() pour \"-1;;;;\" -- (Rien a afficher) */\n");
	puts("Resultat ------------------------------------------------------------");
	afficherMessagesMotif(liste, "-1;;;;");
	puts("");

	puts("\n-- test_rechercheMotifs(): motif present (le motif est un mot complet) --");
	printf("\n /* On appelle rechercheMotifs() pour \"aussi\" */\n");
	puts("Resultat ------------------------------------------------------------");
	afficherMessagesMotif(liste, "aussi");
	puts("");

	puts("\n-- test_rechercheMotifs(): motif present (le motif est une partie d'un mot) --");
	printf("\n /* On appelle rechercheMotifs() pour \"an\" */\n");
	puts("Resultat ------------------------------------------------------------");
	afficherMessagesMotif(liste, "an");

	puts("");

	libererListe(&liste);
}
