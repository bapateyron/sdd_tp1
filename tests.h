/*******************************************************************************
 * 															  TESTS.H
 *
 * Fichier en-tete de tests.c qui declare les prototypes de toutes les fonctions
 * de tests appelees dans le fichier main.c
 *
 ******************************************************************************/
#include "listesChainees.h"

void test_recherchePrecedent(char * nomFichier);
void test_insertionCellules();
void test_lectureFichier(char * nomFichier);
void test_sauvegardeFichier(char * nomFichier);
void test_supprimerCellule(char * nomFichier);
void test_expirationMessages(char * nomFichier);
void test_affichageMessagesValides(char * nomFichier);
void test_modificationDateDebut(char * nomFichier);
void test_rechercheMotifs(char * nomFichier);
