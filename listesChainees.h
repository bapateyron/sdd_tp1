/*******************************************************************************
* 															LISTESCHAINEES.H
*
* Fichier en-tete de main.c qui declare les prototypes de toutes les fonctions
* gerant la manipulation des listes chainees de messages
*
******************************************************************************/

#define _GNU_SOURCE	/* Pour declarer la fonction popen inconnue en norme ansi */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Structure	*****************************************************************/
typedef struct cellule_t {
	int 								dateDebut;	/* aaaammjj */
	int									dateFin;
	char								message[100];
	struct cellule_t	* suivant;
} cellule_t;

/* Prototypes	*****************************************************************/
cellule_t		*	creerCellule(char * message, int dateDebut, int dateFin);
cellule_t		**rechercherDateDebutPrecedente(cellule_t ** pliste, int date);
void					insererCellule(cellule_t ** pliste, cellule_t * cellule);
void					supprimerCellule(cellule_t ** prec);
void					ecrireCellule(FILE * flux, cellule_t * cellule);
void					ecrireListe(FILE * flux, cellule_t * liste);
void					afficherListe(cellule_t * liste);
cellule_t		*	importerListe(char * nomFichier);
void					sauvegardeFichier(char * nomFichierDestination, cellule_t * liste);
int						dateDuJour();
void					expirationMessages(cellule_t ** pliste);
void 					modificationDateDebut(cellule_t ** pliste, int dateAModifier, int nouvelleDate);
void					afficherMessagesValides(cellule_t * liste);
void					libererListe(cellule_t ** pliste);

/******************************************************************************/
/*------------------------------- BONUS -------------------------------------	*/
/******************************************************************************/
int						avoirMotif(char * message, char * motif);
void					afficherMessagesMotif(cellule_t * a0, char * motif);

/******************************************************************************/
